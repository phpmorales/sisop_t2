#
# TRABALHO DE SISTEMAS OPERACIONAS
# IMPLMENTAÇÃO DE UM SISTEMA DE ARQUIVOS
#
# 2013/2
# PERO HENRIQUE PINTO MORALES
#
# arquivo makefile possui a compilação e criação da biblioteca t2fslib.a. 
# Uma observação importante é que não foi possível ligar o código gerado com a lib libapidisk fornecida. 
# Somente foi possível, extraindo os arquivos de dentro da libapidisk. 
#
# arquivo makefile2 possui a compilação completa da biblioteca e posteriormente a compilação com um programa cliente. 
# Ele pode ser alterado através da edição deste arquivo, na linha 13.
# gcc -o caller src/[client.c] -L./lib -lt2fs -Wall -g
#
# O desenvolviemento do projeto e código fonte pode ser visto em:
#	https://bitbucket.org/phpmorales/sisop_t2
#
# Para baixar o código basta ter o pacote git instalado e executar o comando:
#	git clone https://bitbucket.org/phpmorales/sisop_t2.git
