#ifndef t2fs_file

/*
* Color definitions for printf.
*/
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/*
* Definition fo bit states.
*/
#define OFF 0
#define ON  1

/*
* Definitiion of parity.
*/
#define ODD  0
#define EVEN 1

/*
* Definition of return values.
*/
#define POSITIVE  1
#define NEGATIVE  0
#define SUCCESS   0
#define ERROR     NEGATIVE-POSITIVE

/*
* Definition of width of signals.
*/
#define BIT_WIDTH   1
#define BYTE_WIDTH  8
#define WORD_WIDTH  32

/*
* Definition of validity.
*/
#define NAME_VALIDITY_BIT 128

/*
* Definition of descritors limit.
*/
#define DESCRITORS_LIMIT    20

/*
* Definition of data lenght.
*/
#define NAME_EXTEND_LENGHT  40
#define DATA_POINTER_LENGHT 2

/*
* Definition of initial values.
*/
#define INITIAL_POSITION    0
#define INITIAL_SIZE        0
#define STANDARD_INITIAL    0

/*
* Definition os position of each descritor feature.
*/
#define SB_CTRL_SIZE        0
#define SB_DISK_SIZE        1
#define SB_BLOCK_SIZE       2
#define SB_FREE_BLOCK_SIZE  3
#define SB_ROOT_SIZE        4
#define SB_FILE_ENTRY       5

/*
* Definition of value of each descritos feature.
*/
#define CTRL_SIZE       1
#define DISK_SIZE       256
#define BLOCK_SIZE      256
#define FREE_BLOCK_SIZE 1
#define ROOT_SIZE       16
#define FILE_ENTRY      64

/*
* Definition of a file id.
*/
typedef int t2fs_file;

/*
* Structure that saves a register features.
*/
typedef struct {
    unsigned char name[40];
    unsigned int blocksFileSize;
    unsigned int bytesFileSize;
    unsigned int dataPtr[2];
    unsigned int singleIndPtr;
    unsigned int doubleIndPtr;
} __attribute__((packed)) t2fs_record;

/*
* Structure that saves the features of a found register.
*/
typedef struct {
} t2fs_find;

/*
* Structure that define a file descritor.
*/
typedef struct st_descritor_t {
    int block;
    int blockPosition;
    int currentPos;
    t2fs_record fsRecord;
    t2fs_file handler;
} descritor_t;

/******************************************************************************
 * Objective: Identify the developers.
 * Input: Empty.
 * Output: Pointer to string that contain the names.
 ******************************************************************************/
char *t2fs_identify(void);


/******************************************************************************
 * Objective: Function to create a new file.
 * Input: The pointer to a string file name.
 * Output: Handle of a created file or -1 if error.
 ******************************************************************************/
t2fs_file t2fs_create(char *name);


/******************************************************************************
 * Objective: To delete a file.
 * Input: Pointer to a string file name.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_delete(char *name);


/******************************************************************************
 * Objective: To open an existent file on disk.
 * Input: Pointer to a string file name.
 * Output: Handle of a created file or -1 if error.
 ******************************************************************************/
t2fs_file t2fs_open(char *name);


/******************************************************************************
 * Objective: To close a open file.
 * Input: File handle.
 * Output: 0 if sucess or -1 if error.
 ******************************************************************************/
int t2fs_close(t2fs_file handle);


/******************************************************************************
 * Objective: To read size bytes of a file identified by handle.
 * Input: File handle, pointer to buffer and size.
 * Output: the number of read bytes or -1 if error.
 ******************************************************************************/
int t2fs_read(t2fs_file handle, char *buffer, int size);


/******************************************************************************
 * Objective: To write size bytes in a file identified by handle.
 * Input: File handle, pointer to buffer and size.
 * Output: the number of read bytes or -1 if error.
 ******************************************************************************/
int t2fs_write(t2fs_file handle, char *buffer, int size);


/******************************************************************************
 * Objective: To change the counter position of file identified by handle.
 * Input: File handle, offset inside the file.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_seek(t2fs_file handle, unsigned int offset);


/******************************************************************************
 * Objective: Search the first valid register in current directory.
 * Input: Pointer to struct that saves register information.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_first(t2fs_find *findStruct);


/******************************************************************************
 * Objective: Search the next valid register in current directory.
 * Input: Pointer to struct that saves register information an other to dir info.
 * Output: 0 if success or 1 if end of dir or -1 if error.
 ******************************************************************************/
int t2fs_next(t2fs_find *findStruct, t2fs_record *dirFile);


/******************************************************************************
 * Objective: Read the descritor structure of a file.
 * Input: File hanfle.
 * Output: Empty.
 ******************************************************************************/
descritor_t* t2fs_get_descritor(t2fs_file handle);


/******************************************************************************
 * Objective: Set and Reset a bitmap bit.
 * Input: Bitmap bit position and current status.
 * Output: 0 if sucess, -1 if error.
 ******************************************************************************/
int t2fs_set_bitmap(int posicao, short int ocupado);


/******************************************************************************
 * Objective: Allocate a block in data area
 * Input: Empty.
 * Output: block position or -1 if error
 ******************************************************************************/
int t2fs_block_alloc();


/******************************************************************************
 * Objective: Verify if file identified by name exists.
 * Input: File name and pointer to file position.
 * Output: file position or 0 if not found.
 ******************************************************************************/
int t2fs_file_exists(char *name, int *posicao);


/******************************************************************************
 * Objective: Read the disk information strcuture.
 * Input: Emtpy.
 * Output: 0 if sucess or critical error if not a t2fs disk.
 ******************************************************************************/
int t2fs_get_disk_info();


/******************************************************************************
 * Objective: Insert a record into a block file.
 * Input: pointer to a fsRecord structure of a file.
 * Output: 0 if sucess.
 ******************************************************************************/
int t2fs_insert_file_record(t2fs_record* fsRecord);


/******************************************************************************
 * Objective: Extend the name to max lenght.
 * Input: Pointer to file name.
 * Output: Pointer to extended name.
 ******************************************************************************/
char* t2fs_name_extender(char *name);


/******************************************************************************
 * Objective: Get index of file by handle.
 * Input: File handle.
 * Output: file indx or -1 if error.
 ******************************************************************************/
int t2fs_get_index(int handle);


/******************************************************************************
 * Objective: Check if limits of descritors was reached.
 * Input: Empty.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_check_descritors_limit();

/******************************************************************************
 * Objective: Calculate de max file size.
 * Input: Empty.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_calc_file_max_size(int size);


/******************************************************************************
 * Objective: Check if limit of file size was reached.
 * Input: File size, initial size.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_check_file_max_size(int size, int initialSize);


/******************************************************************************
 * Objective: Operation to get a module of a number.
 * Input: Integer number.
 * Output: Module of integer number.
 ******************************************************************************/
int t2fs_math_module(int number);


/******************************************************************************
 * Objective: Operation to get a inverse of a number.
 * Input: Integer number.
 * Output: Inverse of integer number.
 ******************************************************************************/
int t2fs_math_inv(int number);


/******************************************************************************
 * Objective: Check the parity of a number.
 * Input: Integer number.
 * Output: 0 if odd, 1 if even.
 ******************************************************************************/
int t2fs_math_parity(int number);


/******************************************************************************
 * Objective: Potency math operation.
 * Input: Base, expoent.
 * Output: Potency calculated.
 ******************************************************************************/
int t2fs_math_pow(int base, int expoent);


/******************************************************************************
 * Objective: Exit the program due a critical error
 * Input: Empty.
 * Output: Empty.
 ******************************************************************************/
void t2fs_critical_error();


/******************************************************************************
 * Objective: Free the memory allocated for descritors
 * Input: Empty.
 * Output: Empty.
 ******************************************************************************/
void t2fs_exit(void);


#endif
