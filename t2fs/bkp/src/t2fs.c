#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/t2fs.h"
//#include "../include/superblock.h"
#include "../include/apidisk.h"

descritor_t* open_descritors[20];
unsigned char count_descritors = 0;
t2fs_file next_handler = 0;

int sB_diskGeometry = 0;
char sB_ctrlSize = 1;
int sB_diskSize = 256;
int sB_blockSize = 256;
int sB_freeBlockSize = 1;
int sB_rootSize = 16;
int sB_fileEntry = 64;

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
char *t2fs_identify(void){
  return NULL;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
t2fs_file t2fs_create (char *name)
{
    GetDiskInformation();

    if(count_descritors >= 20)
    {
        printf("***********ERRO: Voce ja possui 20 arquivos abertos!\n");
        return -1;
    }

	int retorno;	
	retorno = t2fs_delete(name);
	if (retorno == 0)
	{
		printf("O arquivo anterior de mesmo name foi excluído.\n");
	}

    char *_name;
    _name = ExtendName(name);

    descritor_t* t = (descritor_t*)malloc(sizeof(descritor_t));
    memcpy(t->record.name, _name, 40);//sizeof(name));
    t->record.name[39] = 0;
    t->record.blocksFileSize = 0;
    t->record.bytesFileSize = 0;
    t->record.dataPtr[0] = 0;
    t->record.dataPtr[1] = 0;
    t->record.singleIndPtr = 0;
    t->record.doubleIndPtr = 0;

    InsertFileRecord(&t->record);

    t->handler = next_handler;

    next_handler++;

    open_descritors[count_descritors] = t;
    count_descritors++;

    return t->handler;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int t2fs_delete (char *name)
{
	GetDiskInformation();
	char block[sB_blockSize];
	
	int handle, qtdBlocos, i=0, descIndex;
	handle = t2fs_open(name);
	if (handle<0) return -1;

	descIndex = getIndexByHandle(handle);
	qtdBlocos = open_descritors[descIndex]->record.blocksFileSize;

	while(qtdBlocos > 0 && i<2)
	{
		setBitBitmap(open_descritors[descIndex]->record.dataPtr[i], 0);
		i++;		
		qtdBlocos--;
	}
	
	i = 0;
	if(qtdBlocos > 0)
	{
		setBitBitmap(open_descritors[descIndex]->record.singleIndPtr, 0);
		read_block(open_descritors[descIndex]->record.singleIndPtr, block);
		while(qtdBlocos > 0 && i<sB_blockSize)
		{
			setBitBitmap(block[i], 0);
			i++;		
			qtdBlocos--;
		}
	}

	i = 0;
	if(qtdBlocos > 0)
	{
		setBitBitmap(open_descritors[descIndex]->record.doubleIndPtr, 0);
		read_block(open_descritors[descIndex]->record.doubleIndPtr, block);
		/*while(qtdBlocos > 0 && i<sB_blockSize)     //liberar blocos de indirecao dupla
		{
			setBitBitmap(block[i], 0);
			i++;		
			qtdBlocos--;
		}*/
	}
	
	read_block(open_descritors[descIndex]->bloco, block);	
	block[open_descritors[descIndex]->posNoBloco] = 0;	//colocar 0 no primeiro bit do name do arquivo
	write_block(open_descritors[descIndex]->bloco, block);	
	
	t2fs_close(handle);
	
	return 0;
}


/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
t2fs_file t2fs_open (char *name)
{
    GetDiskInformation();

    if(count_descritors >= 20)
    {
        printf("\n******ERRO: Voce ja possui 20 arquivos abertos. \n");
        return -1;
    }

    int i=0;
		int iBloco = 0;
		int lenBlkCtrl = 0;
    char block[sB_blockSize];

    lenBlkCtrl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    name = ExtendName(name);
    *name = *name | 128; //Liga o bit 7 para fazer a pesquisa

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)  //varre o diretório raiz
    {
        read_block(lenBlkCtrl + iBloco, block);   //lê o bloco
        for (i = 0; i < sB_blockSize; i+=64)       //varre o bloco lido
        {
            //printf("i = %d arquivo: %s e Nome pesquisa: %s\n", i, block + i, name);
            if(strcmp(block + i, name) == 0)  //compara pelo name
            {
                printf("encontrado no bloco %d\n", lenBlkCtrl + iBloco);
                descritor_t* t = (descritor_t*)malloc(sizeof(descritor_t));
								t->bloco = lenBlkCtrl + iBloco;
								t->posNoBloco = i;
								t->currentPos = 0;
                t->handler = next_handler;

                next_handler++;

                memcpy(&(t->record), block+i, sizeof(t->record));  //grava o primeiro registro
                open_descritors[count_descritors] = t;
                count_descritors++;
                return t->handler;;
            }
        }
    }
    //printf("\n******ERRO: Não foi possível encontrar o arquivo. \n");
    return - 1;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int t2fs_close (t2fs_file handle)
{
  int i =0;
  int j=0;
  
  for (i = 0; i < 20; ++i)
  {
    if(open_descritors[i]->handler == handle){
            free(open_descritors[i]);
            for(j=i+1;  j<20; j++)
            {
                open_descritors[j - 1] = open_descritors[j];
            }
            count_descritors--;
            break;
        }
    }

	return SUCCESS;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int t2fs_read(t2fs_file handle, char *buffer, int size)	//lê size bytes do arquivo identificado por handle para o buffer
{
	int posAtual, bytesLidos=0, blocoLido=0, posNoBloco, blockAddress, descIndex;
	char block[sB_blockSize];

	descIndex = getIndexByHandle(handle);
	descritor_t* arquivo = open_descritors[descIndex];
	posAtual = arquivo->currentPos;

	while (size>0)
	{
		if (!blocoLido)  //carrega o bloco atual
		{	
			posNoBloco = posAtual % sB_blockSize;
			if (posAtual<sB_blockSize)
				blockAddress = arquivo->record.dataPtr[0];
			else if (posAtual < 2*sB_blockSize)
				blockAddress = arquivo->record.dataPtr[1];
			else if (posAtual > 2*sB_blockSize && posAtual < (2+sB_blockSize)*sB_blockSize)
			{
				int auxInd;
				auxInd = arquivo->record.singleIndPtr;   //bloco de índice (indireção simples)
				char blockInd[sB_blockSize];				
				read_block(auxInd, blockInd);
				auxInd = posAtual/sB_blockSize - 2;
				blockAddress = blockInd[auxInd];
			}
			else
			{
				//indireçao dupla
			}
			read_block(blockAddress, block);		//lê o bloco atual			
			blocoLido = 1;
		}
		
		buffer[bytesLidos] = block[posNoBloco];
		
		posAtual++;
		posNoBloco++;			//Atualiza flags
		if (posNoBloco > sB_blockSize)
			blocoLido = 0;
		bytesLidos++;
		size--;
	}

	arquivo->currentPos = posAtual;
	printf("\nTamanho do arquivo: %d", arquivo->record.bytesFileSize);
	return bytesLidos;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int t2fs_write(t2fs_file handle, char *buffer, int size)	//escreve size bytes do buffer no arquivo identificado por handle
{
	int tamAtual, blockAddress, i=0, j, sizeLeft, spaceLeft, addrPoint, descIndex;
	char block[sB_blockSize];

	descIndex = getIndexByHandle(handle);
	
	printf("\nHandle: %d  - Index: %d", handle, descIndex);

	int tamOriginal = open_descritors[descIndex]->record.bytesFileSize;

	if (size + tamOriginal > 2*sB_blockSize + sB_blockSize*sB_blockSize + sB_blockSize*sB_blockSize*sB_blockSize)
	{
		printf("\nTamanho máximo de arquivo excedido.");
		return -1;
	}

	sizeLeft = size;
	spaceLeft = (open_descritors[descIndex]->record.blocksFileSize * sB_blockSize) - open_descritors[descIndex]->record.bytesFileSize;
	tamAtual = open_descritors[descIndex]->record.bytesFileSize;

	while (sizeLeft >0)
	{
		if (spaceLeft == 0)		//alocar um bloco de dados
		{
			addrPoint = 0;
			blockAddress = allocateBlock();
			if (blockAddress < 1)
			{
				printf("Erro ao alocar bloco.");
				return -1;
			}
			open_descritors[descIndex]->record.blocksFileSize++;
			spaceLeft = sB_blockSize;
			if (tamAtual==0)
				open_descritors[descIndex]->record.dataPtr[0]  = blockAddress;
			else if (tamAtual < 2*sB_blockSize)
				open_descritors[descIndex]->record.dataPtr[1]  = blockAddress;
			else if (tamAtual == 2*sB_blockSize)// && tamAtual < (2+sB_blockSize)*sB_blockSize) //cria o bloco de índice da indireçao simples
			{
				int blockAddressInd;
				blockAddressInd = allocateBlock();   //aloca bloco de índice (indireção simples)
				if (blockAddressInd < 1)
				{
					printf("Erro ao alocar bloco de índice.");
					return -1;
				}
				open_descritors[descIndex]->record.singleIndPtr = blockAddressInd;
				
				char blockPtr[sB_blockSize];
				blockPtr[0] = blockAddress;
				for (j=1; j<sB_blockSize; j++) blockPtr[j] = 0;
				write_block(blockAddressInd, blockPtr);	//grava bloco de índice
			}
			else if ((tamAtual > 2*sB_blockSize) && (tamAtual < (2+sB_blockSize)*sB_blockSize)) //usa o bloco de índice da indireçao simples
			{
				char blockPtr[sB_blockSize];				
				j = open_descritors[descIndex]->record.blocksFileSize - 3;
				read_block(open_descritors[descIndex]->record.singleIndPtr, blockPtr);
				blockPtr[j] = blockAddress;
				//printf("\n%d Ptr:", j);
				//for(j=0; j<256;j++) printf(" %d", blockPtr[j]);
				write_block(open_descritors[descIndex]->record.singleIndPtr, blockPtr);	//grava bloco de índice
			}
			else
			{
				//indireçao dupla
			}			
		}
		else		//localiza o último bloco de dados do arquivo
		{
			addrPoint = tamAtual % sB_blockSize;
			if (tamAtual<sB_blockSize)
				blockAddress = open_descritors[descIndex]->record.dataPtr[0];
			else if (tamAtual < 2*sB_blockSize)
				blockAddress = open_descritors[descIndex]->record.dataPtr[1];
			else if (tamAtual > 2*sB_blockSize && tamAtual < (2+sB_blockSize)*sB_blockSize)
			{
				int auxInd;
				auxInd = open_descritors[descIndex]->record.singleIndPtr;   //bloco de índice (indireção simples)
				char blockInd[sB_blockSize];				
				read_block(auxInd, blockInd);
				//auxInd = (tamAtual - 2*sB_blockSize) / sB_blockSize;
				auxInd = (open_descritors[descIndex]->record.blocksFileSize) - 3;
				blockAddress = blockInd[auxInd];
//printf("\nQtd. blocos: %d, auxInd: %d, blockAddress: %d", open_descritors[descIndex]->record.blocksFileSize, auxInd, blockAddress);
			}
			else
			{
				//indireçao dupla
			}
			read_block(blockAddress, block);		//lê o último bloco			
		}
		//printf("\n%d - ", blockAddress);
		while (addrPoint<sB_blockSize && sizeLeft>0) 		//preenche o bloco
		{
			block[addrPoint] = buffer[i];
			//printf("%c", buffer[i]);
			i++;
			addrPoint++;
			sizeLeft--;
			spaceLeft--;
		}
		write_block(blockAddress, block);  //escreve dados no disco
		tamAtual = tamOriginal + size - sizeLeft;
	}	
	open_descritors[descIndex]->record.bytesFileSize = tamAtual;
	open_descritors[descIndex]->record.blocksFileSize = tamAtual / sB_blockSize;
	if (addrPoint<sB_blockSize) open_descritors[descIndex]->record.blocksFileSize++; //atualiza descritor
		
	read_block(open_descritors[descIndex]->bloco, block);
	//printf("Conteúdo: \n\n Bloco: %d", open_descritors[descIndex]->bloco);	
	memcpy(block+open_descritors[descIndex]->posNoBloco, &(open_descritors[descIndex]->record), 64);
	write_block(open_descritors[descIndex]->bloco, block);		//atualiza record no root
	
	return size;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
//posiciona o contador na posiçao do offset dentro do arquivo
int t2fs_seek (t2fs_file handle, unsigned int offset)
{
    descritor_t* rec = getdescritor_ByHandle(handle);
    if(rec == NULL)
    {
        printf("\n*******ERRO: Hande invalido!\n");
        return -1;
    }
    if(rec->record.bytesFileSize < offset)
    {
        printf("\n*******ERRO: Offset invalido!\n Offset: %d, Tam. arquivo: %d\n", offset, rec->record.bytesFileSize);
        return -1;
    }
    rec->currentPos = offset;
    return 0;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
//localiza o primeiro arquivo válido do diretório
int t2fs_first (t2fs_find *find_struct)
{
	return 0;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
//obtém o próximo registro válido do diretório
int t2fs_next (t2fs_find *findStruct, t2fs_record *dirFile)
{
	return 0;
}


/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
descritor_t* getdescritor_ByHandle(t2fs_file handle)
{
    int i;
    for(i=0; i<count_descritors; i++)
    {
        if(open_descritors[i]->handler == handle){
            return open_descritors[i];
        }
    }
    return NULL;
}


/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int setBitBitmap(int posicao, short int ocupado)   //seta ou reseta um bit do bitmap
{
    char block[sB_blockSize];
	int iBloco, posBit, posByte, iAux;

	iBloco = sB_ctrlSize + posicao/(8*sB_blockSize);		//posição do bloco que contém o bit desejado
	//printf("\nBit no bloco: %d ", iBloco);

    read_block(iBloco, block);        //lê o bloco

	iAux = (posicao - ((iBloco-1) * sB_blockSize));

	posByte = iAux / 8;

	posBit = 7-(iAux % 8);

	char auxByte;
	auxByte = block[posByte];
	//printf("\nBloco antes: %x - 1 deslocado: %d - posBit: %d ", block[posByte], (1 << posBit), posBit);
	if (ocupado)
		block[posByte] = auxByte | (1 << posBit);
	else
		block[posByte] = auxByte & (254 << posBit);

	//printf("\nBloco depois: %x ", block[posByte]);
	//block[posByte] = ; //setar o bit

	//printf(" - Bit %d do byte: %d\n", posBit, posByte);

	write_block(iBloco, block);       //escreve no disco

    return 0;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int allocateBlock()    //Aloca um bloco da área de dados e índices retornando seu endereço
{
	char block[sB_blockSize], auxByte;
	int i, posByte, posBit, bitmapBlock, inicioDados;

	bitmapBlock = sB_ctrlSize;
	inicioDados = sB_ctrlSize + sB_freeBlockSize + sB_rootSize;

	read_block(bitmapBlock, block);        //lê o bloco
	for (i = inicioDados; i < sB_diskSize; i++) //varre o bitmap
	{
		if (i > (bitmapBlock-sB_ctrlSize+1)*sB_blockSize*8)  //caso exista mais de um bloco de bitmap
		{
			bitmapBlock++;
			read_block(bitmapBlock, block);
		}
		posByte = (i - (bitmapBlock-sB_ctrlSize)) / 8;
		posBit = 7 - (i % 8);

		auxByte = block[posByte] & (1 << posBit);

	//printf("\nByte lido: %x - 1 desloc.: %x - posBit: %d - i: %d - posByte: %d", block[posByte], (1 << posBit), posBit, i, posByte);

		if (auxByte == 0)
		{
			setBitBitmap(i, 1);
			//printf("\nBloco %d alocado.\n", i);
			return i;
		}
	}
	return -1;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int fileExists(char *name, int *posicao)
{
    int i=0, iBloco = 0, lenBlkCtrl = 0;
	int j;
    char block[sB_blockSize];
    lenBlkCtrl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)  //varre o diretório raiz
    {
        read_block(lenBlkCtrl + iBloco, block);   //lê o bloco
        for (i = 0; i < sB_blockSize; i+=64)		 //varre o bloco lido
        {
	    	char fileName[40];
	    	fileName[0] = block[i] - 128;
	    	for(j=1; j<40; j++)
				fileName[j] = block[i+j];

			//printf("\nComparando %s com %s\n", name, fileName);
        	if(!strcmp(name, fileName))  //se o registro tem o name procurado, retorna sua posição
       		{
				//printf("\nJá existe na posição %d ", (iBloco) * sB_blockSize + i);
				*posicao = i;
				return (lenBlkCtrl + iBloco);
            }
        }
    }
	//printf("\nNão existe");
    return 0;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
void InvalidateRootDirectory()
{
    int i=0, dirty = 0, iBloco = 0, lengthBlocoControl = 0;
    char block[sB_blockSize];

    lengthBlocoControl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)	//varre a área de diretório
    {
        read_block(lengthBlocoControl + iBloco, block);   //lê o bloco
        for(i=0; i<sB_blockSize;i+=64)			//varre o bloco
        {
            if(block[i] < (char)161 || block[i] > (char)250)       //se for arquivo válido
            {
                //printf("Valor do primeiro caracter: %d\n", block[i]);
		//printf("Posição %d invalidada.\n", i);
            	block[i] = 0;				//invalida o arquivo
                dirty = 1;
            }
        }
        if(dirty)
	{
	    //printf("Bloco %d gravado.\n", lengthBlocoControl + iBloco);
            write_block(lengthBlocoControl + iBloco, block);  //escreve no disco
	}
    }
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
void GetDiskInformation()
{
    if(!sB_diskGeometry)
    {
        sB_diskGeometry = 1;
        char block[256];
        read_block(0,block);
        if(block[0] == 'T' && block[1] == '2' && block[2] == 'F' && block[3] == 'S')
        {
            sB_ctrlSize = block[5];
            sB_diskSize = *((int *)(block + 6));
            sB_blockSize = *((short int *)(block + 10));
            sB_freeBlockSize = *((short int *)(block + 12));
            sB_rootSize = *((short int *)(block + 14));
            sB_fileEntry = *((short int *)(block + 16));

            //InvalidateRootDirectory();
        }
        else
        {
            printf("\n Not a T2FS disk!!!! \n");
            exit(1);
        }
    }
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int InsertFileRecord(t2fs_record* record)
{
    int i=0, dirty = 0, iBloco = 0, lenBlkCtrl = 0;
    char block[sB_blockSize];
    lenBlkCtrl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)  //varre o diretório raiz
    {
        read_block(lenBlkCtrl + iBloco, block);   //lê o bloco
        for (i = 0; i < sB_blockSize; i+=64)		//varre o bloco lido
        {
            if((unsigned char)block[i] < 161 || (unsigned char)block[i] > 250)  //se o registro não for válido, grava o novo registro
            {
                memcpy(block+i, record, sizeof(*record));  //grava o primeiro registro
                block[i] += 128;		//soma 128 no primeiro caracter
                dirty = 1;
                break;
            }
        }
        if(dirty)
        {
            //printf("Salvo no bloco %d\n", lenBlkCtrl + iBloco);
            write_block(lenBlkCtrl + iBloco, block);       //escreve no disco
			setBitBitmap(iBloco + lenBlkCtrl, 1);
			break;
        }
    }

    return 0;
}

/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
char* ExtendName(char *name)
{
    char* extName = (char *)malloc(40);
    int i = 0;

    while (name[i] != 0)
    {
		extName[i] = name[i];
		i++;
	}
	while (i<40)
    {
		extName[i] = 0;
		i++;
	}
	return (extName);
}


/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
int getIndexByHandle(int handle)
{
	int i=0;
	while(i<20)
	{
		if (open_descritors[i]->handler == handle)
		{
			return i;
		}
		i++;
	}
	return -1;
}


/******************************************************************************
* Objective: 
* Input:
* Output::	
******************************************************************************/
void t2fs_exit(void)
{
    int i = 0;
    printf("\ncount_descritors = %d\n", count_descritors);
    for (i = 0; i < count_descritors; ++i)
    {
        free(open_descritors[i]);
    }
}
