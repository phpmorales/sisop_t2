#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/t2fs.h"
#include "../include/t2fs_plugin.h"
#include "../include/apidisk.h"
#include "../include/superblock.h"

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
descritor_t* getdescritor_ByHandle(t2fs_file handle)
{
    int i;
    for(i=0; i<count_descritores; i++)
    {
        if(descritores_abertos[i]->handler == handle){
            return descritores_abertos[i];
        }
    }
    return NULL;
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
int setBitBitmap(int posicao, short int ocupado)   //seta ou reseta um bit do bitmap
{
    char block[sB_blockSize];
	int iBloco, posBit, posByte, iAux;

	iBloco = sB_ctrlSize + posicao/(8*sB_blockSize);		//posição do bloco que contém o bit desejado
	//printf("\nBit no bloco: %d ", iBloco);

  read_block(iBloco, block);        //lê o bloco

	iAux = (posicao - ((iBloco-1) * sB_blockSize));

	posByte = iAux / 8;

	posBit = 7-(iAux % 8);

	char auxByte;
	auxByte = block[posByte];
	//printf("\nBloco antes: %x - 1 deslocado: %d - posBit: %d ", block[posByte], (1 << posBit), posBit);
	if (ocupado)
		block[posByte] = auxByte | (1 << posBit);
	else
		block[posByte] = auxByte & (254 << posBit);

	//printf("\nBloco depois: %x ", block[posByte]);
	//block[posByte] = ; //setar o bit

	//printf(" - Bit %d do byte: %d\n", posBit, posByte);

	write_block(iBloco, block);       //escreve no disco

    return 0;
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
int allocateBlock()    //Aloca um bloco da área de dados e índices retornando seu endereço
{
	char block[sB_blockSize], auxByte;
	int i, posByte, posBit, bitmapBlock, inicioDados;

	bitmapBlock = sB_ctrlSize;
	inicioDados = sB_ctrlSize + sB_freeBlockSize + sB_rootSize;

	read_block(bitmapBlock, block);        //lê o bloco
	for (i = inicioDados; i < sB_diskSize; i++) //varre o bitmap
	{
		if (i > (bitmapBlock-sB_ctrlSize+1)*sB_blockSize*8)  //caso exista mais de um bloco de bitmap
		{
			bitmapBlock++;
			read_block(bitmapBlock, block);
		}
		posByte = (i - (bitmapBlock- sB_ctrlSize)) / 8;
		posBit = 7 - (i % 8);

		auxByte = block[posByte] & (1 << posBit);

	//printf("\nByte lido: %x - 1 desloc.: %x - posBit: %d - i: %d - posByte: %d", block[posByte], (1 << posBit), posBit, i, posByte);

		if (auxByte == 0)
		{
			setBitBitmap(i, 1);
			//printf("\nBloco %d alocado.\n", i);
			return i;
		}
	}
	return -1;
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
int fileExists(char *name, int *posicao)
{
    int i=0, iBloco = 0, lenBlkCtrl = 0;
	int j;
    char block[sB_blockSize];
    lenBlkCtrl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)  //varre o diretório raiz
    {
        read_block(lenBlkCtrl + iBloco, block);   //lê o bloco
        for (i = 0; i < sB_blockSize; i+=64)		 //varre o bloco lido
        {
	    	char fileName[40];
	    	fileName[0] = block[i] - 128;
	    	for(j=1; j<40; j++)
				fileName[j] = block[i+j];

			//printf("\nComparando %s com %s\n", name, fileName);
        	if(!strcmp(name, fileName))  //se o registro tem o name procurado, retorna sua posição
       		{
				//printf("\nJá existe na posição %d ", (iBloco) * sB_blockSize + i);
				*posicao = i;
				return (lenBlkCtrl + iBloco);
            }
        }
    }
	//printf("\nNão existe");
    return 0;
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
void InvalidateRootDirectory()
{
    int i=0, dirty = 0, iBloco = 0, lengthBlocoControl = 0;
    char block[sB_blockSize];

    lengthBlocoControl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)	//varre a área de diretório
    {
        read_block(lengthBlocoControl + iBloco, block);   //lê o bloco
        for(i=0; i<sB_blockSize;i+=64)			//varre o bloco
        {
            if(block[i] < (char)161 || block[i] > (char)250)       //se for arquivo válido
            {
                //printf("Valor do primeiro caracter: %d\n", block[i]);
		//printf("Posição %d invalidada.\n", i);
            	block[i] = 0;				//invalida o arquivo
                dirty = 1;
            }
        }
        if(dirty)
	{
	    //printf("Bloco %d gravado.\n", lengthBlocoControl + iBloco);
            write_block(lengthBlocoControl + iBloco, block);  //escreve no disco
	}
    }
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
void GetDiskInformation()
{
    if(!sB_diskGeometry)
    {
        sB_diskGeometry = 1;
        char block[256];
        read_block(0,block);
        if(block[0] == 'T' && block[1] == '2' && block[2] == 'F' && block[3] == 'S')
        {
            sB_ctrlSize = block[5];
            sB_diskSize = *((int *)(block + 6));
            sB_blockSize = *((short int *)(block + 10));
            sB_freeBlockSize = *((short int *)(block + 12));
            sB_rootSize = *((short int *)(block + 14));
            sB_fileEntry = *((short int *)(block + 16));

            //InvalidateRootDirectory();
        }
        else
        {
            printf("\n Not a T2FS disk!!!! \n");
            exit(1);
        }
    }
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
int InsertFileRecord(t2fs_record* record)
{
    int i=0, dirty = 0, iBloco = 0, lenBlkCtrl = 0;
    char block[sB_blockSize];
    lenBlkCtrl = sB_ctrlSize + sB_freeBlockSize;    //offset para posição do root

    for(iBloco = 0; iBloco < sB_rootSize; iBloco++)  //varre o diretório raiz
    {
        read_block(lenBlkCtrl + iBloco, block);   //lê o bloco
        for (i = 0; i < sB_blockSize; i+=64)		//varre o bloco lido
        {
            if((unsigned char)block[i] < 161 || (unsigned char)block[i] > 250)  //se o registro não for válido, grava o novo registro
            {
                memcpy(block+i, record, sizeof(*record));  //grava o primeiro registro
                block[i] += 128;		//soma 128 no primeiro caracter
                dirty = 1;
                break;
            }
        }
        if(dirty)
        {
            //printf("Salvo no bloco %d\n", lenBlkCtrl + iBloco);
            write_block(lenBlkCtrl + iBloco, block);       //escreve no disco
			setBitBitmap(iBloco + lenBlkCtrl, 1);
			break;
        }
    }

    return 0;
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
char* ExtendName(char *name)
{
    char* extName = (char *)malloc(40);
    int i = 0;

    while (name[i] != 0)
    {
		extName[i] = name[i];
		i++;
	}
	while (i<40)
    {
		extName[i] = 0;
		i++;
	}
	return (extName);
}

/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
int getIndexByHandle(int handle)
{
	int i=0;
	while(i<20)
	{
		if (descritores_abertos[i]->handler == handle)
		{
			return i;
		}
		i++;
	}
	return -1;
}



/******************************************************************************
* Objective: 
* Entrada:
* Saída::	
******************************************************************************/
void t2fs_exit(void)
{
    int i = 0;
    printf("\ncount_descritores = %d\n", count_descritores);
    for (i = 0; i < count_descritores; ++i)
    {
        free(descritores_abertos[i]);
    }
}

#endif
