#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../include/t2fs.h"
#include "../include/apidisk.h"

/* Global variables */
int sbFeatures[6] = {
    CTRL_SIZE,
    DISK_SIZE,
    BLOCK_SIZE,
    FREE_BLOCK_SIZE,
    ROOT_SIZE,
    FILE_ENTRY
};
descritor_t* openDescritors[DESCRITORS_LIMIT];
int countDescritors = STANDARD_INITIAL;
t2fs_file nextHandler = STANDARD_INITIAL;


/******************************************************************************
 * Objective: Identify the developers.
 * Input: Empty.
 * Output: Pointer to string that contain the names.
 ******************************************************************************/
char *t2fs_identify(void) {
    char *devolepersId;
    char developer1[NAME_EXTEND_LENGHT];
    char developer2[NAME_EXTEND_LENGHT];
    
    devolepersId = malloc (2 * NAME_EXTEND_LENGHT * sizeof (char));
   
    printf("\n\n");
    strcpy (developer1, "143049 - Pedro Morales\n");
    strcpy (developer2, "XXXXXX - Jordano Schutz\n\n\0");
    
    devolepersId = strcat(developer1, developer2);
    
    printf(ANSI_COLOR_GREEN "%s" ANSI_COLOR_WHITE, devolepersId);
    
    return devolepersId;
}

/******************************************************************************
 * Objective: Function to create a new file.
 * Input: The pointer to a string file name.
 * Output: Handle of a created file or -1 if error.
 ******************************************************************************/
t2fs_file t2fs_create(char *name) {
    if (t2fs_check_descritors_limit()) {
        printf(ANSI_COLOR_RED "ERROR: Already %d files open!\n" ANSI_COLOR_WHITE, DESCRITORS_LIMIT);
        return ERROR;
    }

    if (!t2fs_delete(name)) {
        printf(ANSI_COLOR_RED "The file was deleted!\n" ANSI_COLOR_WHITE);
    }

    t2fs_get_disk_info();

    char *extName;
    extName = t2fs_name_extender(name);
    
    descritor_t* newDescritor = (descritor_t*) malloc(sizeof (descritor_t));

    memcpy(newDescritor->fsRecord.name, extName, NAME_EXTEND_LENGHT);
    newDescritor->fsRecord.name[NAME_EXTEND_LENGHT - 1] = STANDARD_INITIAL;
    newDescritor->fsRecord.blocksFileSize = STANDARD_INITIAL;
    newDescritor->fsRecord.bytesFileSize = STANDARD_INITIAL;
    newDescritor->fsRecord.dataPtr[0] = STANDARD_INITIAL;
    newDescritor->fsRecord.dataPtr[1] = STANDARD_INITIAL;
    newDescritor->fsRecord.singleIndPtr = STANDARD_INITIAL;
    newDescritor->fsRecord.doubleIndPtr = STANDARD_INITIAL;

    t2fs_insert_file_record(&newDescritor->fsRecord);

    newDescritor->handler = nextHandler;
    nextHandler++;

    openDescritors[countDescritors] = newDescritor;
    countDescritors++;

    return newDescritor->handler;
}

/******************************************************************************
 * Objective: To delete a file.
 * Input: Pointer to a string file name.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_delete(char *name) {
    t2fs_get_disk_info();
    
    char block[sbFeatures[SB_BLOCK_SIZE]];
    int handle;
    int numBlocks;
    int descritorIndex;
    int blockPosition = STANDARD_INITIAL;
    int dataPointerPosition = STANDARD_INITIAL;
     
    handle = t2fs_open(name);
    if (handle < STANDARD_INITIAL){ 
      return ERROR;
    }
    
    descritorIndex = t2fs_get_index(handle);
    numBlocks = openDescritors[descritorIndex]->fsRecord.blocksFileSize;

    while (numBlocks > STANDARD_INITIAL && dataPointerPosition < DATA_POINTER_LENGHT) {
        t2fs_set_bitmap(openDescritors[descritorIndex]->fsRecord.dataPtr[dataPointerPosition], OFF);
        dataPointerPosition++;
        numBlocks--;
    }

    blockPosition = STANDARD_INITIAL;
    if (numBlocks > STANDARD_INITIAL) {
        t2fs_set_bitmap(openDescritors[descritorIndex]->fsRecord.singleIndPtr, OFF);
        read_block(openDescritors[descritorIndex]->fsRecord.singleIndPtr, block);
        
        while (numBlocks > STANDARD_INITIAL && blockPosition < sbFeatures[SB_BLOCK_SIZE]) {
            t2fs_set_bitmap(block[blockPosition], OFF);
            blockPosition++;
            numBlocks--;
        }
    }

    descritorIndex = STANDARD_INITIAL;
    if (numBlocks > STANDARD_INITIAL) {
        t2fs_set_bitmap(openDescritors[descritorIndex]->fsRecord.doubleIndPtr, OFF);
        read_block(openDescritors[descritorIndex]->fsRecord.doubleIndPtr, block);
    }

    read_block(openDescritors[descritorIndex]->block, block);
    block[openDescritors[descritorIndex]->blockPosition] = OFF; //power off bit seven of file name
    write_block(openDescritors[descritorIndex]->block, block);

    t2fs_close(handle);

    return SUCCESS;
}

/******************************************************************************
 * Objective: To open an existent file on disk.
 * Input: Pointer to a string file name.
 * Output: Handle of a created file or -1 if error.
 ******************************************************************************/
t2fs_file t2fs_open(char *name) {
    t2fs_get_disk_info();

    if (t2fs_check_descritors_limit()) {
        printf(ANSI_COLOR_RED "ERROR: Already %d files open!\n" ANSI_COLOR_WHITE, DESCRITORS_LIMIT);
        return ERROR;
    }

    int blockPosition = STANDARD_INITIAL;
    int rbPosition = STANDARD_INITIAL;
    int cbLenght = STANDARD_INITIAL;
    char block[sbFeatures[SB_BLOCK_SIZE]];

    //offset for root position
    cbLenght = sbFeatures[SB_CTRL_SIZE] + sbFeatures[SB_FREE_BLOCK_SIZE]; 

    name = t2fs_name_extender(name);
    
    //power on bit seven of file name
    *name = *name | NAME_VALIDITY_BIT; 

    //scan the root directory
    for (rbPosition = STANDARD_INITIAL; rbPosition < sbFeatures[SB_ROOT_SIZE]; rbPosition++){
        
        //read the block
        read_block(cbLenght + rbPosition, block); 
        
        //scan the block
        for (blockPosition = STANDARD_INITIAL; blockPosition < sbFeatures[SB_BLOCK_SIZE]; blockPosition += FILE_ENTRY){
            
            //name comparison
            if (!strcmp(block + blockPosition, name)){
                printf("Found in blockblock %d\n", cbLenght + rbPosition);
                descritor_t* currentDescritor = (descritor_t*) malloc(sizeof (descritor_t));
                currentDescritor->block = cbLenght + rbPosition;
                currentDescritor->blockPosition = blockPosition;
                currentDescritor->currentPos = STANDARD_INITIAL;
                currentDescritor->handler = nextHandler;

                nextHandler++;

                //record the first register
                memcpy(&(currentDescritor->fsRecord), block + blockPosition, sizeof (currentDescritor->fsRecord)); 
                openDescritors[countDescritors] = currentDescritor;
                countDescritors++;
                
                return currentDescritor->handler;
            }
        }
    }
    return ERROR;
}

/******************************************************************************
 * Objective: To close a open file.
 * Input: File handle.
 * Output: 0 if sucess or -1 if error.
 ******************************************************************************/
int t2fs_close(t2fs_file handle) {
    int descritorPosition = STANDARD_INITIAL; 
    
    while(descritorPosition < DESCRITORS_LIMIT && openDescritors[descritorPosition]->handler != handle ){ 
      descritorPosition++;
    }
    
    if(openDescritors[descritorPosition]->handler != handle){
      return ERROR;
    }
    
    free(openDescritors[descritorPosition]);  
      
    for (descritorPosition = descritorPosition + 1; descritorPosition < DESCRITORS_LIMIT; descritorPosition++) {
      openDescritors[descritorPosition - 1] = openDescritors[descritorPosition];
    }
    
    countDescritors--;
    
    return SUCCESS;
}

/******************************************************************************
 * Objective: To read size bytes of a file identified by handle.
 * Input: File handle, pointer to buffer and size.
 * Output: the number of read bytes or -1 if error.
 ******************************************************************************/
int t2fs_read(t2fs_file handle, char *buffer, int size){ 
    int bytePosition;
    int readBytes = STANDARD_INITIAL;
    
    int blockRead = STANDARD_INITIAL; 
    int blockPosition;
    int blockAddress;
    
    int descritorIndex;
    
    char block[sbFeatures[SB_BLOCK_SIZE]];

    descritorIndex = t2fs_get_index(handle);
    descritor_t* currentFile = openDescritors[descritorIndex];
    bytePosition = currentFile->currentPos;

    while (size > STANDARD_INITIAL) {
        
        //load block
        if (!blockRead){ 
            blockPosition = bytePosition % sbFeatures[SB_BLOCK_SIZE];
            
            if (bytePosition < sbFeatures[SB_BLOCK_SIZE]){
                blockAddress = currentFile->fsRecord.dataPtr[0];
            }
            else if (bytePosition < DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]){
                blockAddress = currentFile->fsRecord.dataPtr[1];
            }
            else if (bytePosition > DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE] && bytePosition < (DATA_POINTER_LENGHT + sbFeatures[SB_BLOCK_SIZE]) * sbFeatures[SB_BLOCK_SIZE]) {
                
                //block index - simple indirection
                int auxiliarIndex;
                auxiliarIndex = currentFile->fsRecord.singleIndPtr; 
                
                char blockInd[sbFeatures[SB_BLOCK_SIZE]];
                read_block(auxiliarIndex, blockInd);
                
                auxiliarIndex = bytePosition / sbFeatures[SB_BLOCK_SIZE] - DATA_POINTER_LENGHT;
                blockAddress = blockInd[auxiliarIndex];
                
            } else {
            
                //block index - double indirection
                //int auxiliarIndex;
                //auxiliarIndex = currentFile->fsRecord.doubleIndPtr; 
                
            }
            
            //read the block
            read_block(blockAddress, block);          
            blockRead = ON;
        }

        buffer[readBytes] = block[blockPosition];

        //refresh control flags
        bytePosition++;
        blockPosition++;
        
        if (blockPosition > sbFeatures[SB_BLOCK_SIZE]){
            blockRead = OFF;
        }
        
        readBytes++;
        size--;
    }

    currentFile->currentPos = bytePosition;
    printf("\nFile size: %d", currentFile->fsRecord.bytesFileSize);
    
    return readBytes;
}

/******************************************************************************
 * Objective: To write size bytes in a file identified by handle.
 * Input: File handle, pointer to buffer and size.
 * Output: the number of read bytes or -1 if error.
 ******************************************************************************/
int t2fs_write(t2fs_file handle, char *buffer, int size) {
      
    int blockAddress = STANDARD_INITIAL;
    int blockPosition = STANDARD_INITIAL;
    int bufferPosition = STANDARD_INITIAL;
    int currentSize;
    int sizeLeft;
    int spaceLeft; 
    int addressPointer; 
    int descritorIndex;
    char block[sbFeatures[SB_BLOCK_SIZE]];

    descritorIndex = t2fs_get_index(handle);

    printf("\nHandle: %d  - Index: %d", handle, descritorIndex);

    int initialSize = openDescritors[descritorIndex]->fsRecord.bytesFileSize;

    if(t2fs_check_file_max_size(size, initialSize)){
        printf("\nFile maximium size reached!");
        return ERROR;
    }

    sizeLeft = size;
    spaceLeft = (openDescritors[descritorIndex]->fsRecord.blocksFileSize * sbFeatures[SB_BLOCK_SIZE]) - openDescritors[descritorIndex]->fsRecord.bytesFileSize;
    currentSize = openDescritors[descritorIndex]->fsRecord.bytesFileSize;

    while (sizeLeft > STANDARD_INITIAL) {
    
        //alloc a data block
        if (spaceLeft == STANDARD_INITIAL){ 
            addressPointer = STANDARD_INITIAL;
            blockAddress = t2fs_block_alloc();
            
            if (blockAddress < 1) {
                printf("Allocate block error!!");
                return ERROR;
            }
            
            openDescritors[descritorIndex]->fsRecord.blocksFileSize++;
            spaceLeft = sbFeatures[SB_BLOCK_SIZE];
            
            if (currentSize == STANDARD_INITIAL){
                openDescritors[descritorIndex]->fsRecord.dataPtr[0] = blockAddress;
            }
            else if (currentSize < DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]){
                openDescritors[descritorIndex]->fsRecord.dataPtr[1] = blockAddress;
            }
            else if (currentSize == DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]){
            
                //aloc an index block - simple indirection
                int blockAddressInd = t2fs_block_alloc();
                
                if (blockAddressInd < 1) {
                    printf("Error in block index allocation!");
                    return ERROR;
                }
                
                openDescritors[descritorIndex]->fsRecord.singleIndPtr = blockAddressInd;

                char blockPtr[sbFeatures[SB_BLOCK_SIZE]];
                blockPtr[0] = blockAddress;
                
                for (blockPosition = 1; blockPosition < sbFeatures[SB_BLOCK_SIZE]; blockPosition++){ 
                  blockPtr[blockPosition] = STANDARD_INITIAL;
                }
                
                //write a index block
                write_block(blockAddressInd, blockPtr); 
            
            }
            else if ((currentSize > DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]) && (currentSize < (DATA_POINTER_LENGHT + sbFeatures[SB_BLOCK_SIZE]) * sbFeatures[SB_BLOCK_SIZE])){ 
                
                //index block of simple indirection
                char blockPtr[sbFeatures[SB_BLOCK_SIZE]];
                
                blockPosition = openDescritors[descritorIndex]->fsRecord.blocksFileSize - 3;
                
                read_block(openDescritors[descritorIndex]->fsRecord.singleIndPtr, blockPtr);
                
                blockPtr[blockPosition] = blockAddress;
                
                //write a index block
                write_block(openDescritors[descritorIndex]->fsRecord.singleIndPtr, blockPtr);
                
            } else {
            
                //index block of double indirection
                //char blockPtr[sbFeatures[SB_BLOCK_SIZE]];
                
            }
        }
        else{
            //locate the last data block of file
            addressPointer = currentSize % sbFeatures[SB_BLOCK_SIZE];
            
            if (currentSize < sbFeatures[SB_BLOCK_SIZE]){
                blockAddress = openDescritors[descritorIndex]->fsRecord.dataPtr[0];
            }
            else if (currentSize < DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]){
                blockAddress = openDescritors[descritorIndex]->fsRecord.dataPtr[1];
            }
            else if (currentSize > DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE] && currentSize < (DATA_POINTER_LENGHT + sbFeatures[SB_BLOCK_SIZE]) * sbFeatures[SB_BLOCK_SIZE]) {
                
                //block index - simple indirection
                int auxiliarIndex = openDescritors[descritorIndex]->fsRecord.singleIndPtr; 
                char blockInd[sbFeatures[SB_BLOCK_SIZE]];
                
                read_block(auxiliarIndex, blockInd);
                auxiliarIndex = (openDescritors[descritorIndex]->fsRecord.blocksFileSize) - 3;
                blockAddress = blockInd[auxiliarIndex];
                
            } else {
            
                //block index - double indirection
                //int auxiliarIndex = openDescritors[descritorIndex]->fsRecord.doubleIndPtr; 
                //char blockInd[sbFeatures[SB_BLOCK_SIZE]];
                
            }
            
            //read the last block   
            read_block(blockAddress, block);         
        }
        
        //fill the block
        while (addressPointer < sbFeatures[SB_BLOCK_SIZE] && sizeLeft > STANDARD_INITIAL){
            block[addressPointer] = buffer[bufferPosition];
            bufferPosition++;
            addressPointer++;
            sizeLeft--;
            spaceLeft--;
        }
        
        write_block(blockAddress, block); //write on disk
        currentSize = initialSize + size - sizeLeft;
    }
    
    openDescritors[descritorIndex]->fsRecord.bytesFileSize = currentSize;
    openDescritors[descritorIndex]->fsRecord.blocksFileSize = currentSize / sbFeatures[SB_BLOCK_SIZE];
    
    if (addressPointer < sbFeatures[SB_BLOCK_SIZE]){ 
      //refresh descritor
      openDescritors[descritorIndex]->fsRecord.blocksFileSize++; 
    }

    read_block(openDescritors[descritorIndex]->block, block); 
    memcpy(block + openDescritors[descritorIndex]->blockPosition, &(openDescritors[descritorIndex]->fsRecord), FILE_ENTRY);
    
    //refresh fsRecord in root
    write_block(openDescritors[descritorIndex]->block, block);

    return size;
}

/******************************************************************************
 * Objective: To change the counter position of file identified by handle.
 * Input: File handle, offset inside the file.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_seek(t2fs_file handle, unsigned int offset) {
    descritor_t* currentFile = t2fs_get_descritor(handle);
    
    if (currentFile == NULL) {
        printf("\n*******ERROR: Invalid Handle!\n");
        return ERROR;
    }
    if (currentFile->fsRecord.bytesFileSize < currentFile->currentPos + offset) {
        printf("\n*******ERROR: Invalid Offset!\n Offset: %d, File size: %d\n", offset, currentFile->fsRecord.bytesFileSize);
        return ERROR;
    }
    
    currentFile->currentPos =+ offset;
    
    return SUCCESS;
}

/******************************************************************************
 * Objective: Search the first valid register in current directory.
 * Input: Pointer to struct that saves register information.
 * Output: 0 if success or -1 if error.
 ******************************************************************************/
int t2fs_first(t2fs_find *find_struct) {
    return SUCCESS;
}

/******************************************************************************
 * Objective: Search the next valid register in current directory.
 * Input: Pointer to struct that saves register information an other to dir info.
 * Output: 0 if success or 1 if end of dir or -1 if error.
 ******************************************************************************/
int t2fs_next(t2fs_find *findStruct, t2fs_record *dirFile) {
    return SUCCESS;
}

/******************************************************************************
 * Objective: Read the descritor structure of a file.
 * Input: File hanfle.
 * Output: Empty.
 ******************************************************************************/
descritor_t* t2fs_get_descritor(t2fs_file handle) {
    int descritorPosition;
    
    for (descritorPosition = INITIAL_POSITION; descritorPosition < countDescritors; descritorPosition++) {
        if (openDescritors[descritorPosition]->handler == handle) {
            return openDescritors[descritorPosition];
        }
    }
    return NULL;
}

/******************************************************************************
 * Objective: Set and Reset a bitmap bit.
 * Input: Bitmap bit position and current status.
 * Output: 0 if sucess, -1 if error.
 ******************************************************************************/
int t2fs_set_bitmap(int posicao, short int ocupado){
    char block[sbFeatures[SB_BLOCK_SIZE]];
    int rbPosition;
    int bitPosition;
    int bytePosition; 
    int auxiliarIndex;
    char auxiliarByte;

    //block position that contain the bit
    rbPosition = sbFeatures[SB_CTRL_SIZE] + posicao / (BYTE_WIDTH * sbFeatures[SB_BLOCK_SIZE]); 
    
    //read the block
    read_block(rbPosition, block); 

    auxiliarIndex = (posicao - ((rbPosition - BIT_WIDTH) * sbFeatures[SB_BLOCK_SIZE]));

    bytePosition = auxiliarIndex / BYTE_WIDTH;

    bitPosition = (BYTE_WIDTH - BIT_WIDTH) - (auxiliarIndex % BYTE_WIDTH);

    auxiliarByte = block[bytePosition];

    if (ocupado){
        block[bytePosition] = auxiliarByte | (BIT_WIDTH << bitPosition);
    }
    else if(!ocupado){
        block[bytePosition] = auxiliarByte & ( (BLOCK_SIZE - 2) << bitPosition);
    }
    else{
      return ERROR;
    }
    
    write_block(rbPosition, block); //write on disk

    return SUCCESS;
}

/******************************************************************************
 * Objective: Allocate a block in data area
 * Input: Empty.
 * Output: block position or -1 if error
 ******************************************************************************/
int t2fs_block_alloc(){
    char block[sbFeatures[SB_BLOCK_SIZE]];
    char auxiliarByte;
    int blockPosition;
    int bytePosition;
    int bitPosition;
    int bitmapBlock;
    int firstRegister;

    bitmapBlock = sbFeatures[SB_CTRL_SIZE];
    firstRegister = sbFeatures[SB_CTRL_SIZE] + sbFeatures[SB_FREE_BLOCK_SIZE] + sbFeatures[SB_ROOT_SIZE];

    //read the block
    read_block(bitmapBlock, block); 
    
    //scan the bitmap block
    for (blockPosition = firstRegister; blockPosition < sbFeatures[SB_DISK_SIZE]; blockPosition++){
    
        //if more than on bitmap block
        if (blockPosition > (bitmapBlock - sbFeatures[SB_CTRL_SIZE] + BIT_WIDTH) * sbFeatures[SB_BLOCK_SIZE] * BYTE_WIDTH){ 
            bitmapBlock++;
            read_block(bitmapBlock, block);
        }
        
        bytePosition = (blockPosition - (bitmapBlock - sbFeatures[SB_CTRL_SIZE])) / BYTE_WIDTH;
        bitPosition = (BYTE_WIDTH - BIT_WIDTH) - (blockPosition % BYTE_WIDTH);

        auxiliarByte = block[bytePosition] & (BIT_WIDTH << bitPosition);

        if (auxiliarByte == STANDARD_INITIAL) {
            t2fs_set_bitmap(blockPosition, ON);
            return blockPosition;
        }
    }
    
    return ERROR;
}

/******************************************************************************
 * Objective: Verify if file identified by name exists.
 * Input: File name and pointer to file position.
 * Output: file position or 0 if not found.
 ******************************************************************************/
int t2fs_file_exists(char *name, int *posicao) {
    int blockPosition = STANDARD_INITIAL;
    int rbPosition = STANDARD_INITIAL; 
    int cbLenght = STANDARD_INITIAL;
    int namePosition;
    char block[sbFeatures[SB_BLOCK_SIZE]];
    char fileName[NAME_EXTEND_LENGHT];
    
    //offset for root position
    cbLenght = sbFeatures[SB_CTRL_SIZE] + sbFeatures[SB_FREE_BLOCK_SIZE]; 

    //scan the root directory
    for (rbPosition = STANDARD_INITIAL; rbPosition < sbFeatures[SB_ROOT_SIZE]; rbPosition++){
        
        //read the block
        read_block(cbLenght + rbPosition, block); 
       
        //scan the block
        for (blockPosition = STANDARD_INITIAL; blockPosition < sbFeatures[SB_BLOCK_SIZE]; blockPosition += FILE_ENTRY){
            
            fileName[0] = block[blockPosition] - NAME_VALIDITY_BIT;
            
            for (namePosition = STANDARD_INITIAL; namePosition < NAME_EXTEND_LENGHT; namePosition++)
                fileName[namePosition] = block[blockPosition + namePosition];

            //if the register have the same name, return the block position
            if (!strcmp(name, fileName)){
                *posicao = blockPosition;
                return (cbLenght + rbPosition);
            }
        }
    }

    return NEGATIVE;
}

/******************************************************************************
 * Objective: Read the disk information strcuture.
 * Input: Emtpy.
 * Output: 0 if sucess or critical error if not a t2fs disk.
 ******************************************************************************/
int t2fs_get_disk_info() {

  char block[256];
  read_block(0, block);
        
  if (!strncmp(block, "T2FS", 4)){
    sbFeatures[SB_CTRL_SIZE] = block[5];
    sbFeatures[SB_DISK_SIZE] = *((int *) (block + 6));
    sbFeatures[SB_BLOCK_SIZE] = *((short int *) (block + 10));
    sbFeatures[SB_FREE_BLOCK_SIZE] = *((short int *) (block + 12));
    sbFeatures[SB_ROOT_SIZE] = *((short int *) (block + 14));
    sbFeatures[SB_FILE_ENTRY] = *((short int *) (block + 16));
 }
 else {
    printf("\nIs not a T2FS disk! \n");
    t2fs_critical_error();
 }
  return SUCCESS;
}

/******************************************************************************
 * Objective: Insert a record into a block file.
 * Input: pointer to a fsRecord structure of a file.
 * Output: 0 if sucess.
 ******************************************************************************/
int t2fs_insert_file_record(t2fs_record* fsRecord) {
    int blockPosition = STANDARD_INITIAL;
    int trashFlag = STANDARD_INITIAL; // flag
    int rbPosition = STANDARD_INITIAL; // root block position
    int cbLenght = STANDARD_INITIAL; // control block lenght
    char block[sbFeatures[SB_BLOCK_SIZE]];
    
    //offset for root position
    cbLenght = sbFeatures[SB_CTRL_SIZE] + sbFeatures[SB_FREE_BLOCK_SIZE]; 

    //scan the root directory
    for (rbPosition = STANDARD_INITIAL; rbPosition < sbFeatures[SB_ROOT_SIZE]; rbPosition++){ 
    
        //read the block
        read_block(cbLenght + rbPosition, block);
        
        //scan the block
        for (blockPosition = STANDARD_INITIAL; blockPosition < sbFeatures[SB_BLOCK_SIZE]; blockPosition += FILE_ENTRY){ 
            
            //if invalid register, record a new register
            if ((unsigned char) block[blockPosition] < 161 || (unsigned char) block[blockPosition] > 250){
                
                //record the first register
                memcpy(block + blockPosition, fsRecord, sizeof (*fsRecord)); 
                
                //power on the bit seven in the first character
                block[blockPosition] += NAME_VALIDITY_BIT; 
                trashFlag = 1;
                break;
            }
        }
        if (trashFlag){
            //write on disk
            write_block(cbLenght + rbPosition, block); 
            t2fs_set_bitmap(rbPosition + cbLenght, ON);
            break;
        }
    }

    return SUCCESS;
}

/******************************************************************************
 * Objective: Extend the name to max lenght.
 * Input: Pointer to file name.
 * Output: Pointer to extended name.
 ******************************************************************************/
char* t2fs_name_extender(char *name) {
    char* extName = (char *) malloc(NAME_EXTEND_LENGHT);
    int namePosition = STANDARD_INITIAL;

    while (name[namePosition] != STANDARD_INITIAL) {
        extName[namePosition] = name[namePosition];
        namePosition++;
    }
    while (namePosition < NAME_EXTEND_LENGHT) {
        extName[namePosition] = STANDARD_INITIAL;
        namePosition++;
    }
    
    return (extName);
}

/******************************************************************************
 * Objective: Get index of file by handle.
 * Input: File handle.
 * Output: file indx or -1 if error.
 ******************************************************************************/
int t2fs_get_index(int handle) {
    int fileIndex = STANDARD_INITIAL;
    while (fileIndex < DESCRITORS_LIMIT) {
        if (openDescritors[fileIndex]->handler == handle) {
            return fileIndex;
        }
        fileIndex++;
    }
    return ERROR;
}

/******************************************************************************
 * Objective: Check if limit of descritors was reached.
 * Input: Empty.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_check_descritors_limit(){
  if(countDescritors >= DESCRITORS_LIMIT){
    return POSITIVE;
  }
  else{
    return NEGATIVE;
  }
}

/******************************************************************************
 * Objective: Calculate de max file size.
 * Input: Empty.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_calc_file_max_size(int size){
  int maxSize;
  
  maxSize = (DATA_POINTER_LENGHT * sbFeatures[SB_BLOCK_SIZE]) + t2fs_math_pow(sbFeatures[SB_BLOCK_SIZE], 2) + t2fs_math_pow(sbFeatures[SB_BLOCK_SIZE], 3);

  return maxSize;
}

/******************************************************************************
 * Objective: Check if limit of file size was reached.
 * Input: File size, initial size.
 * Output: 1 if yes, 0 if no.
 ******************************************************************************/
int t2fs_check_file_max_size(int size, int initialSize){
    
    if (size + initialSize > t2fs_calc_file_max_size(size)) {
      return POSITIVE;
    }
    else{
      return NEGATIVE;
    }
}

/******************************************************************************
 * Objective: Operation to get a module of a number.
 * Input: Integer number.
 * Output: Module of integer number.
 ******************************************************************************/
int t2fs_math_module(int number){
    int numberModule = number;

    if(number < 0){
        numberModule = number - 2 * number;
    }

    return numberModule;
}

/******************************************************************************
 * Objective: Operation to get a inverse of a number.
 * Input: Integer number.
 * Output: Inverse of integer number.
 ******************************************************************************/
int t2fs_math_inv(int number){
    int numberInverted;

    numberInverted = number - 2 * number;

    return numberInverted;
}

/******************************************************************************
 * Objective: Check the parity of a number.
 * Input: Integer number.
 * Output: 0 if odd, 1 if even.
 ******************************************************************************/
int t2fs_math_parity(int number){
    if(number % 2 == 0){
        return EVEN;
    }
    else{
        return ODD;
    }
}

/******************************************************************************
 * Objective: Potency math operation.
 * Input: Base, expoent.
 * Output: Potency calculated.
 ******************************************************************************/
int t2fs_math_pow(int base, int expoent){
  int potency = 1;

  if(expoent != 0){

    int expModule = t2fs_math_module(expoent);

    do{
        expModule--;
        potency = potency * base;
    }while(expModule > 0);

    if(expoent < 0 && t2fs_math_parity(expoent)){
        potency = t2fs_math_inv(potency);
    }
  }

  return potency;

}

/******************************************************************************
 * Objective: Exit the program due a critical error
 * Input: Empty.
 * Output: Empty.
 ******************************************************************************/
void t2fs_critical_error(){
  printf( ANSI_COLOR_GREEN "A CRITICAL SYSTEM ERROR OCCURRED!\nPLEASE, VERIFY THE t2fs_disk.dat FILE!\nTHE THE SYSTEM WILL SHUT DOWN!\n" ANSI_COLOR_WHITE);
  exit(1);
}

/******************************************************************************
 * Objective: Free the memory allocated for descritors
 * Input: Empty.
 * Output: Empty.
 ******************************************************************************/
void t2fs_exit() {
    int i = STANDARD_INITIAL;
    printf("\ncountDescritors = %d\n", countDescritors);
    for (i = STANDARD_INITIAL; i < countDescritors; ++i) {
        free(openDescritors[i]);
    }
}


